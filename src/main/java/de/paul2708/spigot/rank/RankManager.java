package de.paul2708.spigot.rank;

import de.paul2708.common.rank.Rank;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.tablist.TablistManager;
import de.paul2708.spigot.util.Settings;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul on 11.08.2016.
 */
public class RankManager {

    @Getter
    private static Map<Player, PermissionAttachment> permissions = new HashMap<>();

    public static void update(Rank rank) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (PlayerAPI.getRank(player) == null) continue;

            if (PlayerAPI.getRank(player).getName().equalsIgnoreCase(rank.getName())) {
                apply(player, rank);
            }
        }
    }

    public static void apply(Player p, Rank rank) {
        if (p == null || !p.isOnline()) {
            return;
        }

        if (rank == null) {
            permissions.remove(p);
            return;
        }

        // Removing permissions
        p.setOp(false);

        if (permissions.get(p) != null) {
            PermissionAttachment attachment = permissions.get(p);
            p.removeAttachment(attachment);
            attachment.remove();

            permissions.remove(p);
        }

        // Apply new rank
        PermissionAttachment attachment = p.addAttachment(CloudSpigot.getInstance());
        for (String permission : rank.getPermissionList()) {
            attachment.setPermission(permission, true);
        }

        permissions.put(p, attachment);

        if (rank.getPermissionLevel() >= 120) {
            p.setOp(true);
        }

        // Tablist
        if (Settings.TABLIST) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(CloudSpigot.getInstance(), () -> {
                if (PlayerAPI.isNicked(p)) {
                    TablistManager.setTablist(p, rank, PlayerAPI.getNick(p));
                } else {
                    TablistManager.setTablist(p, rank, null);
                }
            });
        }
    }
}
