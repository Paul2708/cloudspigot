package de.paul2708.spigot.maintenance;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.util.Util;
import de.paul2708.spigot.util.nms.PacketUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

/**
 * Created by Paul on 07.09.2016.
 */
public class MaintenanceManager {

    private static BukkitTask task;
    private static int currentTime;

    public static void start() {
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(CloudSpigot.getInstance(), () -> {

            String time = Util.secToMin(currentTime);

            for (Player player : Bukkit.getOnlinePlayers()) {
                if (currentTime >= 0) {
                    PacketUtil.setActionBar(player, "§7Wartungen starten in §e" + time + " Minuten");
                } else {
                    PacketUtil.setActionBar(player, "§eWartungen haben begonnen.");
                }
            }

            currentTime--;
        }, 0L, 20L);
    }

    public static void stop() {
        if (task != null) {
            task.cancel();
        }
    }

    public static void setTime(long timeInMillis) {
        currentTime = (int) (timeInMillis/1000);
    }
}
