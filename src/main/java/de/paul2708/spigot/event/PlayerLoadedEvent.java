package de.paul2708.spigot.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Paul on 27.07.2016.
 */
public class PlayerLoadedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private Player player;

    public PlayerLoadedEvent(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
