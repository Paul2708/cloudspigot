package de.paul2708.spigot.chatlog;

import de.paul2708.common.chatlog.Chatlog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Paul on 03.09.2016.
 */
public class ChatlogManager {

    private static Map<UUID, ArrayList<String>> log = new HashMap<>();

    public static void log(UUID uuid, String message, String action) {
        ArrayList<String> old = log.get(uuid);
        if (old == null) {
            old = new ArrayList<>();
        }

        old.add(action + ":" + message + ":" + System.currentTimeMillis());

        log.put(uuid, old);
    }

    public static Chatlog build(String playerUuid, String targetUuid, String playerName,
                                String targetName) {
        UUID uuid = UUID.fromString(targetUuid);
        return new Chatlog(playerUuid, targetUuid, playerName, targetName,
                (log.get(uuid) == null ? new ArrayList<>() : log.get(uuid)),
                System.currentTimeMillis()
                );
    }

}
