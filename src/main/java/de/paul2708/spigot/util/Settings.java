package de.paul2708.spigot.util;

import de.paul2708.common.game.GameState;

/**
 * Created by Paul on 06.05.2017.
 */
public class Settings {

    public static String MOTD;
    public static GameState GAME_STATE;
    public static boolean CAN_JOIN;
    public static boolean TABLIST = true;
}
