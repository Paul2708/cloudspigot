package de.paul2708.spigot.util;

import de.paul2708.common.report.Report;
import de.paul2708.common.report.ReportCause;
import de.paul2708.spigot.api.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 31.08.2016.
 */
public class ItemManager {

    // Inventories
    private static Inventory reportReasonInventory;
    private static Inventory reportInventory;

    public static void openReportReasonInventory(Player p) {
        p.openInventory(reportReasonInventory);
    }

    public static void openReportInventory(Player p) {
        p.openInventory(reportInventory);
    }

    public static void updateReports(List<Report> reports) {
        reportInventory.clear();
        int id = 0;
        for (Report report : reports) {
            ItemStack item = new ItemBuilder().name("§e#" + id + " " + report.getTargetName())
                    .type(Material.SKULL_ITEM)
                    .subID(3)
                    .description("§8-----------------")
                    .description("§7• von §e" + report.getReporterName() + " §7reportet")
                    .description("§7• Grund: §e" + report.getCause().getName())
                    .description("§7• Server: §e" + report.getServerName())
                    .description("§7• Anzahl: §e" + report.getAmount())
                    .description("§7• Zeitpunkt: §e" + Util.getTime(report.getTime()))
                    .description("§8-----------------")
                    .description("§7• Linksklicken, um den Report zu §aübernehmen")
                    .description("§7• Rechtsklicken, um den Report zu §clöschen")
                    .build();
            SkullMeta sm = (SkullMeta) item.clone().getItemMeta();
            sm.setOwner(report.getTargetName());
            item.setItemMeta(sm);

            ItemManager.reportInventory.setItem(id, item);
            id++;
        }
    }

    public static void initializeReasonInventory() {
        reportReasonInventory = Bukkit.createInventory(null, Util.getSize(ReportCause.values().length),
                "§7Report - Gründe");

        int slot = 0;
        for (ReportCause cause : ReportCause.values()) {
            List<String> description = new ArrayList<>();
            description.add("§7-----------------");
            cause.getDescription().forEach(msg -> description.add("§7" + msg));

            ItemBuilder builder = new ItemBuilder()
                    .name("§e" + cause.getName())
                    .id(cause.getItemId());
            description.forEach(builder::description);

            reportReasonInventory.setItem(slot, builder.build());
            slot++;
        }
    }

    public static void initializeReportInventory() {
        reportInventory = Bukkit.createInventory(null, 54, "§7Report-Verwaltung");
    }
}
