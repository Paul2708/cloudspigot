package de.paul2708.spigot.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Paul on 18.08.2016.
 */
public class Util {

    // Time
    private static DateFormat format;

    static {
        format = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");
    }

    public static String getTime(long time) {
        return format.format(new Date(time));
    }

    public static String secToMin(int i) {
        int ms = i / 60;
        int ss = i % 60;
        String m = (ms < 10 ? "0" : "") + ms;
        String s = (ss < 10 ? "0" : "") + ss;
        return m + ":" + s;
    }

    // Inventory size
    public static int getSize(int count) {
        if(count <= 8) return 9;
        if(count <= 17) return 18;
        if(count <= 26) return 27;
        if(count <= 35) return 36;
        if(count <= 44) return 45;
        return -1;
    }
}
