package de.paul2708.spigot.util.nms;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.server.v1_8_R3.PacketDataSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutCustomPayload;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class PacketUtil {

	private static Constructor<?> chatComponentTextConstructor, packetPlayOutChatConstructor;

    static {
		try {
			chatComponentTextConstructor = ReflectionUtil.getNMSClass("ChatComponentText").getConstructor(String.class);
            Class<?> iChatBase = ReflectionUtil.getNMSClass("IChatBaseComponent");
			packetPlayOutChatConstructor = ReflectionUtil.getNMSClass("PacketPlayOutChat").getConstructor(iChatBase);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void setActionBar(Player p, String message) {
        try {
            Object o = chatComponentTextConstructor.newInstance(message);
            Object packet = packetPlayOutChatConstructor.newInstance(o);
            Field field = packet.getClass().getDeclaredField("b");
            field.setAccessible(true);
            field.set(packet, (byte) 2);
            
			ReflectionUtil.sendPacket(p, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// https://www.spigotmc.org/threads/tutorial-open-a-book-serverside.96101/
	public static void openBook(ItemStack book, Player p) {
		int slot = p.getInventory().getHeldItemSlot();
		ItemStack old = p.getInventory().getItem(slot);
		p.getInventory().setItem(slot, book);

		ByteBuf buf = Unpooled.buffer(256);
		buf.setByte(0, (byte)0);
		buf.writerIndex(1);

		PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload("MC|BOpen", new PacketDataSerializer(buf));
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
		p.getInventory().setItem(slot, old);
	}

}
