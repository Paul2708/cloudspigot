package de.paul2708.spigot.util.nms;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ReflectionUtil {
	
	public static void sendPacket(Player p, Object packet) throws Exception {
		Object handle = p.getClass().getMethod("getHandle").invoke(p);
		Object connection = handle.getClass().getField("playerConnection").get(handle);
		connection.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(connection, packet);
	}

	public static Class<?> getNMSClass(String name) {
		String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		try {
			return Class.forName("net.minecraft.server." + version + "." + name);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
}
