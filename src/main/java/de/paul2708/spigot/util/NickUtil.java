package de.paul2708.spigot.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.paul2708.common.nick.Skin;
import de.paul2708.spigot.CloudSpigot;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

/**
 * Created by Paul on 07.06.2017.
 */
public class NickUtil {

    public static void setSkinAndName(Player player, Skin skin, String nick) {
        CraftPlayer craftPlayer = (CraftPlayer) player;

        GameProfile profile = craftPlayer.getProfile();
        profile.getProperties().clear();
        profile.getProperties().put("textures", new Property("textures", skin.getValue(), skin.getSignature()));

        try {
            Field field = getField(GameProfile.class, "name");
            field.set(profile, nick);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        destroy(player);
        removeFromTablist(player);

        Bukkit.getScheduler().scheduleSyncDelayedTask(CloudSpigot.getInstance(), () -> {
            spawn(player);
            addToTablist(player);
        }, 4L);
    }

    private static void spawn(Player player) {
        CraftPlayer cp = (CraftPlayer) player;
        for (Player all : Bukkit.getOnlinePlayers()) {
            if (!all.getName().equalsIgnoreCase(player.getName())) {
                PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
                ((CraftPlayer) all).getHandle().playerConnection.sendPacket(spawn);
            }
        }
    }

    private static void destroy(Player player) {
        CraftPlayer cp = (CraftPlayer) player;
        PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(cp.getEntityId());
        sendPacket(destroy);
    }

    private static void addToTablist(Player player) {
        CraftPlayer cp = (CraftPlayer) player;
        if (player != null && player.isOnline()) {
            PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(
                    PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, cp.getHandle());
            sendPacket(packet);
        }
    }

    private static void removeFromTablist(Player player) {
        CraftPlayer cp = (CraftPlayer) player;
        PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(
                PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
        sendPacket(packet);
    }

    private static void sendPacket(Packet<?> packet) {
        for (Player all : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet);
        }
    }

    private static Field getField(Class<?> clazz, String name) {
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException|SecurityException e) {
            e.printStackTrace();
        }
        return null;
    }
}
