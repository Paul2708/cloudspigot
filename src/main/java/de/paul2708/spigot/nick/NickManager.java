package de.paul2708.spigot.nick;

import de.paul2708.common.nick.Nick;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.tablist.TablistManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 04.06.2017.
 */
public class NickManager {

    private static Map<UUID, Nick> nicks = new ConcurrentHashMap<>();

    public static void apply(Player player, Nick nick) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(CloudSpigot.getInstance(), () -> {
            Nick oldNick = nicks.get(player.getUniqueId());
            if (oldNick != null) {
                TablistManager.remove(oldNick.getName());
            } else {
                TablistManager.remove(player.getName());
            }

            nicks.put(player.getUniqueId(), nick);

            TablistManager.setTablist(player, null, nick.getName());

            // Apply nick
            /*Skin skin = nick.getSkin();

            NickUtil.setSkinAndName(player, skin, "§a" + nick.getName());

            Bukkit.broadcastMessage("Name: " + player.getName());*/
        });
    }

    public static void remove(Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(CloudSpigot.getInstance(), () -> {
            Nick oldNick = nicks.get(player.getUniqueId());
            if (oldNick != null) {
                TablistManager.remove(oldNick.getName());
            }

            nicks.remove(player.getUniqueId());

            // Remove nick
            // NickUtil.setSkinAndName(player, skinCache.get(player.getUniqueId()), player.getName());

            TablistManager.setTablist(player, PlayerAPI.getRank(player), null);
        });
    }

    public static boolean isNicked(Player player) {
        return nicks.containsKey(player.getUniqueId());
    }

    public static Nick getNick(Player player) {
        return nicks.get(player.getUniqueId());
    }
}
