package de.paul2708.spigot.api;

import de.paul2708.api.request.Request;
import de.paul2708.api.update.Upgrade;
import de.paul2708.common.api.Information;
import de.paul2708.common.api.Update;
import de.paul2708.common.game.GameState;
import de.paul2708.common.packet.server.ServerJoinPacket;
import de.paul2708.common.packet.server.ServerStatePacket;
import de.paul2708.common.server.ServerData;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.util.Settings;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 31.12.2016.
 */
public class ServerAPI {

    public static int getOnlinePlayers() {
        return CloudSpigot.getCloudAPI().getCache().get(Information.Type.ONLINE_PLAYERS);
    }

    public static void send(Player player, String server) {
        Upgrade upgrade = new Upgrade.Builder()
                .add(Update.Type.SEND, player.getUniqueId(), server)
                .build();
        upgrade.send();
    }

    public static void sendToHub(Player player) {
        Request request = new Request.Builder()
                .identifier("spigot_hub")
                .add(Information.Type.AVAILABLE_SERVER, "Lobby")
                .handle(response -> {
                    ServerData data = response.get(0);

                    if (data.getName().equals("-/-")) {
                        player.kickPlayer(CloudSpigot.TAG + "§cEs konnte keine Lobby gefunden werden.");
                    } else {
                        send(player, data.getName());
                    }
                })
                .build();
        request.send();
    }

    public static void setMotd(String motd) {
        Settings.MOTD = motd;

        Upgrade upgrade = new Upgrade.Builder()
                .add(Update.Type.MOTD, CloudSpigot.getConfigFile().getName(), motd)
                .build();
        upgrade.send();
    }

    public static void setGameState(GameState gameState) {
        Settings.GAME_STATE = gameState;

        CloudSpigot.getCloudAPI().getClient().send(
                new ServerStatePacket(CloudSpigot.getConfigFile().getName(), gameState.getId()));
    }

    public static GameState getState() {
        return Settings.GAME_STATE;
    }

    public static void setJoinAble(boolean canJoin) {
        Settings.CAN_JOIN = canJoin;

        CloudSpigot.getCloudAPI().getClient().send(
                new ServerJoinPacket(CloudSpigot.getConfigFile().getName(), canJoin));
    }

    public static void setTablist(boolean active) {
        Settings.TABLIST = active;
    }

    public static void refresh(long ticks, String... groups) {
        Bukkit.getScheduler().runTaskTimerAsynchronously(CloudSpigot.getInstance(), () -> {

            Request.Builder builder = new Request.Builder()
                    .identifier("api_refresh_task")
                    .handle(response -> {

                    });
            for (int i = 0; i < groups.length; i++) {
                builder.add(Information.Type.AVAILABLE_SERVER, groups[i]);
            }

            builder.build().send();

        }, ticks, ticks);
    }
}
