package de.paul2708.spigot.api;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Hologram {

    private HashMap<Player, List<Integer>> players = new HashMap<Player, List<Integer>>();
    private List<String> lines;
    private Location loc;

    public Hologram(Location loc, List<String> lines) {
        this.lines = lines;
        this.loc = loc;
    }

    public void display(Player p) {
        Location displayLoc = this.loc.clone().add(0.0D,
                0.23D * this.lines.size() - 1.97D, 0.0D);
        WorldServer nmsWorld = ((CraftWorld) displayLoc.getWorld()).getHandle();
        PlayerConnection con = ((CraftPlayer) p).getHandle().playerConnection;
        for (int i = 0; i < this.lines.size(); i++) {
            EntityArmorStand armorStand = getEntity(nmsWorld,
                    displayLoc.getX(), displayLoc.getY(), displayLoc.getZ(),
                    (String) this.lines.get(i));
            con.sendPacket(new PacketPlayOutSpawnEntityLiving(armorStand));
            displayLoc.add(0.0D, -0.23D, 0.0D);

            addPlayer(p, armorStand.getId());
        }
    }

    public void destroy(Player p) {
        PlayerConnection con = ((CraftPlayer) p).getHandle().playerConnection;
        for (Iterator<?> localIterator = getPlayerEntities(p).iterator(); localIterator
                .hasNext();) {
            int id = ((Integer) localIterator.next()).intValue();
            con.sendPacket(new PacketPlayOutEntityDestroy(new int[] { id }));
        }
        removePlayer(p);
    }

    private EntityArmorStand getEntity(WorldServer nmsWorld, double x,
                                       double y, double z, String text) {
        EntityArmorStand armorStand = new EntityArmorStand(nmsWorld);
        armorStand.setLocation(x, y, z, 0.0F, 0.0F);
        armorStand.setCustomName(text);
        armorStand.setCustomNameVisible(true);
        armorStand.setInvisible(true);

        return armorStand;
    }

    private void removePlayer(Player p) {
        this.players.remove(p);
    }

    private void addPlayer(Player p, int entityId) {
        if(this.players.containsKey(p)) {
            ((List<Integer>) this.players.get(p)).add(Integer.valueOf(entityId));
        } else {
            this.players.put(p, new ArrayList<>());
            ((List<Integer>) this.players.get(p)).add(Integer.valueOf(entityId));
        }
    }

    private List<Integer> getPlayerEntities(Player p) {
        return this.players.containsKey(p) ? (List<Integer>) this.players.get(p) : null;
    }
}
