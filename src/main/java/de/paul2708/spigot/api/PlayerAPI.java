package de.paul2708.spigot.api;

import de.paul2708.api.update.Upgrade;
import de.paul2708.common.api.Information;
import de.paul2708.common.api.Update;
import de.paul2708.common.command.CommandType;
import de.paul2708.common.nick.Nick;
import de.paul2708.common.rank.Rank;
import de.paul2708.spigot.CloudSpigot;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 31.12.2016.
 */
public class PlayerAPI {

    public static void performCommand(Player player, CommandType command, String args) {
        Upgrade upgrade = new Upgrade.Builder()
                .add(Update.Type.COMMAND, player.getUniqueId(), command, args)
                .build();
        upgrade.send();
    }

    public static Rank getRank(Player player) {
        return CloudSpigot.getCloudAPI().getCache().get(Information.Type.PLAYER_RANK, player.getUniqueId());
    }

    public static int getPlayTime(Player player) {
        return CloudSpigot.getCloudAPI().getCache().get(Information.Type.PLAY_TIME, player.getUniqueId());
    }

    public static boolean isNicked(Player player) {
        if (!CloudSpigot.getConfigFile().isGameServer()) {
            return false;
        }

        return getNick(player) != null;
    }

    public static String getNick(Player player) {
        Nick nick = CloudSpigot.getCloudAPI().getCache().get(Information.Type.NICK, player.getUniqueId());

        if (nick == null || nick.getName().equalsIgnoreCase("")) {
            return null;
        }

        return nick.getName();
    }
}
