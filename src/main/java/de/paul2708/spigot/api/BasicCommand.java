package de.paul2708.spigot.api;

import de.paul2708.spigot.CloudSpigot;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * Created by Paul on 07.06.2016.
 */
public abstract class BasicCommand implements CommandExecutor {

    // TODO: Add dynamic registration

    private int permissionLevel;

    public BasicCommand(JavaPlugin plugin, String command, int permissionLevel, List<String> aliases) {
        plugin.getCommand(command).setExecutor(this);

        if (aliases != null) {
            CloudSpigot.getInstance().getCommand(command).setAliases(aliases);
        }

        this.permissionLevel = permissionLevel;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (PlayerAPI.getRank(p).getPermissionLevel() >= permissionLevel) {
                run(p, args);
            } else {
                p.sendMessage(CloudSpigot.TAG + "§cDu hast keine Rechte dafür.");
            }

            return true;
        } else {
            sender.sendMessage("[CloudSpigot] Der Command ist nur fuer Spieler nutzbar.");
        }

        return false;
    }

    public abstract void run(Player p, String args[]);
}
