package de.paul2708.spigot.api;


import de.paul2708.spigot.CloudSpigot;
import org.bukkit.Bukkit;

/**
 * Created by Paul on 11.06.2016.
 */
public abstract class BasicCountdown {

    private int id;
    private int time, startTime;
    private boolean running;

    public BasicCountdown(int time) {
        this.time = time;
        this.startTime = time;
        this.running = false;
    }

    public void start() {
        this.running = true;
        this.id = Bukkit.getScheduler().scheduleSyncRepeatingTask(CloudSpigot.getInstance(), () -> {
            time--;
            handle(time);

            if(time == 0) {
                cancel();
            }
        }, 20L, 20L);
    }

    public abstract void handle(int time);

    public void cancel() {
        this.running = false;
        time = startTime;
        Bukkit.getScheduler().cancelTask(id);
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getRemainingTime() {
        return time;
    }

    public boolean isRunning() {
        return running;
    }
}
