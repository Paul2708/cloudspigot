package de.paul2708.spigot.api;

import de.paul2708.spigot.CloudSpigot;
import org.bukkit.Bukkit;

import java.sql.*;

public class MySQL {

	private Connection connect;
	private String host = "localhost";
	private int port = 3306;
	private String database;
	private String user;
	private String password;
	
	public MySQL(String host, String database, String user, String password) {
		this.host = host;
		this.database = database;
		this.user = user;
		this.password = password;
	}

	public Connection openConnection() {
	    try {
	    	System.out.println("Verbindung zur Datenbank '" + this.database + "' wird aufgebaut...");
	      	Class.forName("com.mysql.jdbc.Driver");
	      	this.connect = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password + "&autoReconnect=true");
			System.out.println("Verbindung zur Datenbank '" + this.database + "' hergestellt.");
	    } catch (SQLException e) {
			System.out.println("Verbindung zur Datenbank '" + this.database + "' konnte nicht hergestellt werden.");
	    	e.printStackTrace();
	    } catch (ClassNotFoundException e) {
			System.out.println("Der MySQL-Treiber wurde nicht gefunden!");
	    	e.printStackTrace();
	    }
	    return this.connect;
	}

	public Connection getConnection() {
		return this.connect;
	}

	public boolean hasConnection() {
		try {
			return (this.connect != null) || (this.connect.isValid(1));
		} catch (SQLException e) {
			System.out.println("Fehler beim Abfragen der Verbindung.");
			e.printStackTrace();
		}

		return false;
	}

	public void queryUpdate(String query) {
		Bukkit.getScheduler().runTaskAsynchronously(CloudSpigot.getInstance(), () -> {

            Connection con = MySQL.this.connect;
            PreparedStatement st = null;
            try {
                st = con.prepareStatement(query);
                st.executeUpdate();
            } catch(SQLException e) {
                System.out.println("Fehler beim Ausfuehren einer Query.");
                e.printStackTrace();
            } finally {
                MySQL.this.closeResources(null, st);
            }
        });
	}

  public ResultSet getQuery(String query) {
	  try {
		  PreparedStatement stmt = this.connect.prepareStatement(query);
		  return stmt.executeQuery();
	  } catch(Exception e) {
		  	System.out.println("Fehler beim Bekommen eines ResultSets.");
			e.printStackTrace();
	  }
	  
	  return null;
  }
  
	public void closeResources(ResultSet rs, PreparedStatement st) {
		try {
			if(rs != null) rs.close();
			if(st != null) st.close();
		} catch (SQLException e) {
			System.out.println("Fehler beim Schließen der Resourcen.");
			e.printStackTrace();
		}
	}

	public void closeConnection() {
		try {
			this.connect.close();
		} catch (SQLException e) {
			System.out.println("Fehler beim Schließen der Verbindung.");
			e.printStackTrace();
		} finally {
			this.connect = null;
		}
	}

}
