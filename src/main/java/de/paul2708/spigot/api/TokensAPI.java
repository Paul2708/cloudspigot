package de.paul2708.spigot.api;

import de.paul2708.api.request.Request;
import de.paul2708.api.request.RequestHandler;
import de.paul2708.api.update.Upgrade;
import de.paul2708.common.api.Information;
import de.paul2708.common.api.Update;
import de.paul2708.spigot.CloudSpigot;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 31.12.2016.
 */
public class TokensAPI {

    public static void updateTokens(Player player, int value, String reason, RequestHandler handler) {
        Upgrade upgrade = new Upgrade.Builder()
                .add(Update.Type.TOKENS, player.getUniqueId(), value)
                .build();
        upgrade.send();

        if (reason != null) {
            if (value >= 0) {
                player.sendMessage("§eTokens §8│ §7Du hast §e" + value + " Tokens §7erhalten. (§e" + reason + "§7)");
            } else {
                player.sendMessage("§eTokens §8│ §7Du hast §e" + Math.abs(value) + " Tokens §7abgezogen bekommen. (§e" + reason + "§7)");
            }
        }

        Request.Builder builder = new Request.Builder()
                .identifier("tokens update request")
                .add(Information.Type.TOKENS, player.getUniqueId());
        if (handler != null) {
            builder.handle(handler);
        }

        builder.build().send();
    }

    public static void updateTokens(Player player, int value, String reason) {
        updateTokens(player, value, reason, null);
    }

    public static int getTokens(Player player) {
        return CloudSpigot.getCloudAPI().getCache().get(Information.Type.TOKENS, player.getUniqueId());
    }

}
