package de.paul2708.spigot.report;

import de.paul2708.common.report.Report;
import de.paul2708.spigot.util.ItemManager;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 18.08.2016.
 */
public class ReportManager {

    private static Map<Player, Object[]> reportData = new HashMap<>();
    private static List<Report> currentReports = new CopyOnWriteArrayList<>();

    public static void addData(Player player, String target, String targetServer) {
        reportData.put(player, new Object[] { player.getName(), target, targetServer });
    }

    public static void removeData(Player player) {
        reportData.remove(player);
    }

    public static Object[] getData(Player p) {
        return reportData.get(p);
    }

    public static void update(List<Report> reports) {
        currentReports = reports;
        // Send boss bar
        TextComponent text;
        if (reports.size() == 0) {
            text = new TextComponent("§7Keine Reports sind offen!");
        } else if (reports.size() == 1) {
            text = new TextComponent("§7Ein Report ist offen. -> §e/reports");
        } else {
            text = new TextComponent("§e" + reports.size() + " §7Reports sind offen. -> §e/reports");
        }

        // Update inv
        ItemManager.updateReports(reports);
    }

    public static Report getReport(String targetName) {
        for (Report report : currentReports) {
            if (report.getTargetName().equalsIgnoreCase(targetName)) {
                return report;
            }
        }

        return null;
    }
}
