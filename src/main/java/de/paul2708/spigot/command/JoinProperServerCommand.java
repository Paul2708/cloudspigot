package de.paul2708.spigot.command;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.ServerAPI;
import de.paul2708.spigot.image.ImageManager;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 15.08.2016.
 */
public class JoinProperServerCommand extends BasicCommand {

    public JoinProperServerCommand() {
        super(CloudSpigot.getInstance(), "joinproperserver", 0, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (ImageManager.players.get(p.getUniqueId()) == null) {
            return;
        }

        String server = ImageManager.players.get(p.getUniqueId());

        p.sendMessage(CloudSpigot.TAG + "§7Du wirst auf den Server §e" + server + " §7gesendet...");
        ServerAPI.send(p, server);
    }

}
