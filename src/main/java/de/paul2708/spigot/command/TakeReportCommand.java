package de.paul2708.spigot.command;

import de.paul2708.common.packet.report.RemoveReportPacket;
import de.paul2708.common.report.Report;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.ServerAPI;
import de.paul2708.spigot.report.ReportManager;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 15.08.2016.
 */
public class TakeReportCommand extends BasicCommand {

    public TakeReportCommand() {
        super(CloudSpigot.getInstance(), "takereport", 96, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (args.length != 1) {
            p.sendMessage(CloudSpigot.TAG + "§cKlicke bitte nur auf Nachrichten.");
            return;
        }

        String target = args[0];

        Report report = ReportManager.getReport(target);
        if (report == null) {
            p.sendMessage(CloudSpigot.TAG + "§cDer Report wurde bereits bearbeitet.");
            return;
        }

        CloudSpigot.getCloudAPI().getClient().send(new RemoveReportPacket(report));
        ServerAPI.send(p, report.getServerName());
        p.sendMessage(CloudSpigot.TAG + "§7Du übernimmst den Report.");
    }

}
