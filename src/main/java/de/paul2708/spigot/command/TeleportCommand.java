package de.paul2708.spigot.command;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.PlayerAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Created by Christian on 09.12.2016.
 */
public class TeleportCommand extends BasicCommand {

    public TeleportCommand() {
        super(CloudSpigot.getInstance(), "teleport", 95, Arrays.asList("tp"));
    }

    @Override
    public void run(Player p, String[] args) {
        if (PlayerAPI.getRank(p).getPermissionLevel() != 120 &&
                CloudSpigot.getConfigFile().isGameServer()) {
            p.sendMessage(CloudSpigot.TAG + "§cDu kannst dich nur zu Spielern telepotieren, wenn du nicht auf einem Game-Server bist.");
            return;
        }

        if (args.length == 1) {
            Player target = Bukkit.getPlayer(args[0]);

            if (target != null) {
                p.teleport(target);
                p.sendMessage(CloudSpigot.TAG + "§7Du hast dich zu " + PlayerAPI.getRank(target).getPrefix() + target.getName() + " §7telepotiert.");
            } else {
                p.sendMessage(CloudSpigot.TAG + "§cDer Spieler " + args[0] + " ist offline.");
            }
        } else if (args.length == 2) {
            Player target = Bukkit.getPlayer(args[0]);
            Player otherTarget = Bukkit.getPlayer(args[1]);

            if (target == null) {
                p.sendMessage(CloudSpigot.TAG + "§cDer Spieler " + args[0] + " ist offline.");
                return;
            }
            if (otherTarget == null) {
                p.sendMessage(CloudSpigot.TAG + "§cDer Spieler " + args[1] + " ist offline.");
                return;
            }

            target.teleport(otherTarget);
            p.sendMessage(CloudSpigot.TAG + "§7Du hast den Spieler " + PlayerAPI.getRank(target).getPrefix()  + target.getName() +
                    " §7zu " + PlayerAPI.getRank(otherTarget).getPrefix()  + otherTarget.getName() + " §7telepotiert");

        } else {
            p.sendMessage(CloudSpigot.TAG + "§7Nutze §e/tp <Spieler> [Spieler]");
        }
    }

}
