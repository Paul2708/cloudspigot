package de.paul2708.spigot.command;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.PlayerAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Created by Christian on 08.12.2016.
 */
public class ClearChatCommand extends BasicCommand {

    public ClearChatCommand() {
        super(CloudSpigot.getInstance(), "clearchat", 95, Arrays.asList("cc"));
    }

    @Override
    public void run(Player p, String[] args) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            for (int i = 0; i < 100; i++) {
                player.sendMessage("");
            }

            player.sendMessage(CloudSpigot.TAG + PlayerAPI.getRank(p).getPrefix() + p.getName() +" §ehat den Chat gelöscht.");
        }
    }

}
