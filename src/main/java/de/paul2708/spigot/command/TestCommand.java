package de.paul2708.spigot.command;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 15.08.2016.
 */
public class TestCommand extends BasicCommand {

    public TestCommand() {
        super(CloudSpigot.getInstance(), "test", 0, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (args.length != 1) {
            p.sendMessage("§7Nutze §e/test <Permission>");
        } else {
            if (p.hasPermission(args[0])) {
                if (p.isOp()) {
                    p.sendMessage("§aDu hast die Permission, weil du OP bist.");
                } else {
                    p.sendMessage("§aDu hast die Permission - ohne OP.");
                }
            } else {
                p.sendMessage("§cDu hast die Permission nicht.");
            }
        }
    }

}
