package de.paul2708.spigot.command;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.survey.SurveyManager;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 15.08.2016.
 */
public class OpenBookCommand extends BasicCommand {

    public OpenBookCommand() {
        super(CloudSpigot.getInstance(), "openbook", 10, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (args.length == 0) {
            return;
        }

        if (SurveyManager.getQuestions() != null && SurveyManager.contains(p.getUniqueId())) {
            String answer = "";

            for (int i = 0; i < args.length; i++) {
                answer += args[i] + " ";
            }

            if (answer.equals("")) {
                return;
            }

            answer = answer.substring(0, answer.length() - 1);

            SurveyManager.vote(p, answer);
            SurveyManager.openGui(p);
        }
    }

}
