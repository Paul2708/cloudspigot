package de.paul2708.spigot.command;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.PlayerAPI;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Created by Christian on 09.12.2016.
 */
public class GameModeCommand extends BasicCommand {

    public GameModeCommand() {
        super(CloudSpigot.getInstance(), "gamemode", 95, Arrays.asList("gm"));
    }

    @Override
    public void run(Player p, String[] args) {
        if (PlayerAPI.getRank(p).getPermissionLevel() != 120 && CloudSpigot.getConfigFile().isGameServer()) {
            p.sendMessage(CloudSpigot.TAG + "§cDu kannst deinen Gamemode nur ändern, wenn du nicht auf einem Game-Server bist.");
            return;
        }

        if (args.length == 0 || args.length > 2) {
            p.sendMessage(CloudSpigot.TAG + "§7Nutze §e/gamemode <0|1|2|3> [Spieler]");
            return;
        }

        if(PlayerAPI.getRank(p).getPermissionLevel() == 95 || PlayerAPI.getRank(p).getPermissionLevel() > 110) {
            try {
                int mode = Integer.valueOf(args[0]);
                if (mode < 0 || mode > 3) {
                    p.sendMessage(CloudSpigot.TAG + "§c" + args[0] + " ist kein möglicher Gamemode.");
                    return;
                }

                if (args.length == 1) {
                    p.setGameMode(GameMode.getByValue(mode));
                    p.sendMessage(CloudSpigot.TAG + "§7Du bist nun im Gamemode §e" + mode + "§7.");
                } else {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target == null) {
                        p.sendMessage(CloudSpigot.TAG + "§c" + args[1] + " ist offline");
                        return;
                    }

                    target.setGameMode(GameMode.getByValue(mode));
                    target.sendMessage(CloudSpigot.TAG + "§7Du bist nun im Gamemode §e" + mode + "§7.");
                    p.sendMessage(CloudSpigot.TAG + "§7Du hast " + PlayerAPI.getRank(target).getPrefix() +
                            target.getName() + " §7in den Gamemode §e" + mode + " §7gesetzt.");
                }

            } catch (NumberFormatException e) {
                p.sendMessage(CloudSpigot.TAG + "§c" + args[0] + " ist kein möglicher Gamemode.");
                return;
            }
        }
    }

}
