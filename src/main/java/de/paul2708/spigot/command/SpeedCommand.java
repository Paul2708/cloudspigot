package de.paul2708.spigot.command;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.PlayerAPI;
import org.bukkit.entity.Player;

/**
 * Created by Christian on 09.12.2016.
 */
public class SpeedCommand extends BasicCommand {

    public SpeedCommand() {
        super(CloudSpigot.getInstance(), "speed", 100, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (PlayerAPI.getRank(p).getPermissionLevel() != 120 &&
                CloudSpigot.getConfigFile().isGameServer()) {
            p.sendMessage(CloudSpigot.TAG + "§Du kannst die Geschwindigkeit nur dann ändern, wenn du nicht auf einem Game-Server bist.");
            return;
        }

        if (args.length == 1) {
            try {
                int speed = Integer.parseInt(args[0]);
                if (speed > 11 || speed < 0) {
                    p.sendMessage(CloudSpigot.TAG + "§7Bitte gib eine Geschwindigkeit von §e0 §7bis §e10 §7an.");
                    return;
                }

                p.setFlySpeed((float) (speed * 0.1));
                p.sendMessage(CloudSpigot.TAG + "§7Dein Speed wurde auf §e" + speed + " §7gesetzt.");
            } catch (NumberFormatException ex) {
                p.sendMessage(CloudSpigot.TAG + "§7Bitte gib eine Geschwindigkeit von §e0 §7bis §e10 §7an.");
            }
        } else {
            p.sendMessage(CloudSpigot.TAG + "§7Nutze §e/speed <1-10>");
        }
    }

}
