package de.paul2708.spigot.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.common.packet.report.ListReportPacket;
import de.paul2708.common.packet.report.RequestReportPacket;
import de.paul2708.spigot.report.ReportManager;
import de.paul2708.spigot.util.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 08.10.2016.
 */
public class ReportPacketHandler implements PacketListener {

    @PacketHandler
    public void onRequestPacket(Session session, RequestReportPacket packet) {
        // Request
        Player player = Bukkit.getPlayer(packet.getReporterName());
        ReportManager.addData(player, packet.getTargetName(), packet.getTargetServer());

        ItemManager.openReportReasonInventory(player);
    }

    @PacketHandler
    public void onListPacket(Session session, ListReportPacket packet) {
        // List
        ReportManager.update(packet.getReports());

        if (!packet.getName().equalsIgnoreCase("")) {
            ItemManager.openReportInventory(Bukkit.getPlayer(packet.getName()));
        }
    }
}
