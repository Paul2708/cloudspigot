package de.paul2708.spigot.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.common.chatlog.Chatlog;
import de.paul2708.common.packet.other.ChatlogPacket;
import de.paul2708.common.packet.other.MaintenancePacket;
import de.paul2708.common.packet.player.PlayerImageMessagePacket;
import de.paul2708.common.packet.server.ServerCommandPacket;
import de.paul2708.common.packet.server.ServerKillPacket;
import de.paul2708.common.packet.survey.SurveyOpenPacket;
import de.paul2708.common.survey.Question;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.chatlog.ChatlogManager;
import de.paul2708.spigot.image.ImageManager;
import de.paul2708.spigot.maintenance.MaintenanceManager;
import de.paul2708.spigot.survey.SurveyManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Paul on 08.10.2016.
 */
public class CommandPacketHandler implements PacketListener {

    @PacketHandler
    public void onChatlogPacket(Session session, ChatlogPacket packet) {
        // Chatlog
        Chatlog chatlog = packet.getChatlog();
        CloudSpigot.getCloudAPI().getClient().send(new ChatlogPacket(ChatlogManager.build(chatlog.getPlayerUuid(),
                chatlog.getTargetUuid(), chatlog.getPlayerName(), chatlog.getTargetName())));
    }

    @PacketHandler
    public void onMaintenancePacket(Session session, MaintenancePacket packet) {
        // Maintenance
        if (packet.getOperation().equalsIgnoreCase("disable")) {
            MaintenanceManager.stop();
        } else {
            MaintenanceManager.setTime(packet.getTime());
            MaintenanceManager.start();
        }
    }

    @PacketHandler
    public void onImageMessage(Session session, PlayerImageMessagePacket packet) {
        // Image message
        ImageManager.sendInfo(packet.getUuid(), packet.getName(), packet.getRank(), packet.getServer());
    }

    @PacketHandler
    public void onServerKill(Session session, ServerKillPacket packet) {
        // Stop server
        Bukkit.shutdown();
    }

    @PacketHandler
    public void onCommand(Session session, ServerCommandPacket packet) {
        // Send command
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), packet.getCommand());
    }

    @PacketHandler
    public void onSurveyOpen(Session session, SurveyOpenPacket packet) {
        // Open book gui
        Player player = Bukkit.getPlayer(packet.getUuid());
        List<Question> questions = packet.getQuestions();

        if (questions.size() == 0) {
            for (Player all : Bukkit.getOnlinePlayers()) {
                SurveyManager.remove(all.getUniqueId());
            }

            return;
        }

        if (SurveyManager.getQuestions() == null) {
            for (Question question : questions) {
                question.clear();
            }

            SurveyManager.setQuestions(questions);
        }

        SurveyManager.remove(packet.getUuid());
        SurveyManager.openGui(player);
    }
}
