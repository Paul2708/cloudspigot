package de.paul2708.spigot.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.api.request.Request;
import de.paul2708.common.api.Information;
import de.paul2708.common.packet.rank.PermissionUpdatePacket;
import de.paul2708.common.packet.rank.RankUpdatePacket;
import de.paul2708.spigot.rank.RankManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Paul on 08.10.2016.
 */
public class RankPacketHandler implements PacketListener {

    @PacketHandler
    public void onUpdatePacket(Session session, RankUpdatePacket packet) {
        // Update
        Player player = Bukkit.getPlayer(UUID.fromString(packet.getUuid()));
        if (player == null) {
            return;
        }

        Request request = new Request.Builder()
                .identifier("api_rank_update")
                .add(Information.Type.PLAYER_RANK, player.getUniqueId())
                .handle(response -> RankManager.apply(player, packet.getTo()))
                .build();
        request.send();
    }

    @PacketHandler
    public void onPermissionPacket(Session session, PermissionUpdatePacket packet) {
        // Permission
        RankManager.update(packet.getRank());
    }
}
