package de.paul2708.spigot.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.api.request.Request;
import de.paul2708.common.api.Information;
import de.paul2708.common.nick.Nick;
import de.paul2708.common.packet.other.ClickMessagePacket;
import de.paul2708.common.packet.player.PlayerNickPacket;
import de.paul2708.common.packet.player.PlayerSoundPacket;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.nick.NickManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 08.10.2016.
 */
public class PlayerPacketHandler implements PacketListener {

    @PacketHandler
    public void onSound(Session session, PlayerSoundPacket packet) {
        // Sound
        String soundName = packet.getSound();
        Sound sound = Sound.valueOf(soundName.toUpperCase());
        Player player = Bukkit.getPlayer(packet.getUuid());
        if (player == null || sound == null) {
            return;
        }

        player.playSound(player.getLocation(), sound, 1f, 1f);
    }

    @PacketHandler
    public void onMessage(Session session, ClickMessagePacket packet) {
        // Send message
        Player player = Bukkit.getPlayer(packet.getTo());
        TextComponent text = new TextComponent(packet.getMessage());
        if (!packet.getHoverText().equals("")) {
            text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(packet.getHoverText()).create()));
        }
        if (!packet.getCommand().equals("")) {
            text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, packet.getCommand()));
        }
        if (!packet.getSuggest().equals("")) {
            text.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, packet.getSuggest()));
        }

        player.spigot().sendMessage(text);
    }

    @PacketHandler
    public void onNick(Session session, PlayerNickPacket packet) {
        // Nick
        Nick nick = packet.getNick();
        Player player = Bukkit.getPlayer(packet.getUuid());

        // Request
        Request request = new Request.Builder()
                .identifier("auto_nick_update")
                .add(Information.Type.NICK, player.getUniqueId())
                .build();
        request.send();

        if (CloudSpigot.getConfigFile().isGameServer()) {
            if (nick.getName().equalsIgnoreCase("")) {
                // Remove nick
                NickManager.remove(player);
            } else {
                // Apply
                NickManager.apply(player, nick);
            }
        }
    }
}
