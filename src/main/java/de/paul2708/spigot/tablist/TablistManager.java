package de.paul2708.spigot.tablist;

import de.paul2708.common.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * Created by Paul on 02.04.2016.
 */
public class TablistManager {

    // TODO: Add scoreboard function as api

    private static final int MAX = 500;
    private static Scoreboard scoreboard;

    public static void setTablist(Player p, Rank rank, String nick) {
        if (nick != null) {
            // TODO: set real spieler rank
            rank = new Rank("Spieler", "§aSpieler", "§a", 20, "", -1);

            registerTeam(rank);

            scoreboard.getTeam(id(rank)).addEntry(nick);

            p.setDisplayName("§a" + nick);
            p.setPlayerListName("§a" + nick);
            p.setCustomNameVisible(true);
            p.setCustomName("§a" + nick);
        } else {
            registerTeam(rank);

            for (Team team : scoreboard.getTeams()) {
                for (String entry : team.getEntries()) {
                    if (team.hasEntry(p.getName())) {
                        team.removeEntry(entry);
                    }
                }
            }

            scoreboard.getTeam(id(rank)).addEntry(p.getName());

            p.setDisplayName(scoreboard.getTeam(id(rank)).getPrefix() + p.getName());
            p.setPlayerListName(scoreboard.getTeam(id(rank)).getPrefix() + p.getName());
            p.setCustomNameVisible(true);
            p.setCustomName(scoreboard.getTeam(id(rank)).getPrefix() + p.getName());
        }

        for (Player all : Bukkit.getOnlinePlayers()) {
            all.setScoreboard(scoreboard);
        }
    }

    public static void remove(String name) {
        if (scoreboard == null) {
            return;
        }

        for (Team team : scoreboard.getTeams()) {
            for (String entry : team.getEntries()) {
                if (team.hasEntry(name)) {
                    team.removeEntry(entry);
                }
            }
        }
    }

    private static void registerTeam(Rank rank) {
        if (scoreboard == null) {
            scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        if (scoreboard.getTeam(id(rank)) == null) {
            Team team = scoreboard.registerNewTeam(id(rank));
            team.setNameTagVisibility(NameTagVisibility.ALWAYS);
            team.setPrefix(rank.getShortTag());
        }
    }

    private static String id(Rank rank) {
        return (MAX - rank.getPermissionLevel()) + "";
    }

}
