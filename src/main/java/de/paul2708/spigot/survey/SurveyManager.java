package de.paul2708.spigot.survey;

import de.paul2708.common.packet.survey.SurveyResultPacket;
import de.paul2708.common.survey.Question;
import de.paul2708.common.survey.Result;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.util.nms.PacketUtil;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftMetaBook;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 18.06.2017.
 */
public class SurveyManager {

    private static List<Question> questions;

    private static Map<UUID, Result> results = new ConcurrentHashMap<>();

    public static void setQuestions(List<Question> questions) {
        SurveyManager.questions = questions;
    }

    public static void openGui(Player player) {
        if (!results.containsKey(player.getUniqueId())) {
            Result result = new Result();

            for (int i = 0; i < questions.size(); i++) {
                result.setVote(i, "");
            }

            results.put(player.getUniqueId(), result);
        }

        int index = getLastIndex(player);
        if (index == -1) {
            SurveyResultPacket packet = new SurveyResultPacket(player.getUniqueId(), results.get(player.getUniqueId()));
            CloudSpigot.getCloudAPI().getClient().send(packet);
            remove(player.getUniqueId());
            return;
        }

        Question question = questions.get(index);

        // Book
        ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta bookMeta = (BookMeta) book.getItemMeta();
        List<IChatBaseComponent> pages;

        try {
            pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
        } catch (ReflectiveOperationException ex) {
            ex.printStackTrace();
            return;
        }

        ComponentBuilder builder = new ComponentBuilder("").append("§n§7" + (index + 1) + ". Frage:" + "\n§r" + question.getQuestion() + "\n\n");

        List<String> answers = new ArrayList<>(question.getAnswers().keySet());
        for (int i = 0; i < answers.size(); i++) {
            builder.append("§7" + String.valueOf((char) (i + 97)) + ") §r" + answers.get(i) + "\n");
            builder.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/openbook " + answers.get(i)));
            builder.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Klicke, um die Antwort §e" + String.valueOf((char) (i + 97)) + ") §7zu wählen").create()));
        }

        String json = ComponentSerializer.toString(builder.create());

        IChatBaseComponent page = IChatBaseComponent.ChatSerializer.a(json);
        pages.add(page);

        book.setItemMeta(bookMeta);

        // Open
        PacketUtil.openBook(book, player);
    }

    public static void vote(Player player, String answer) {
        Result result = results.get(player.getUniqueId());

        result.setVote(getLastIndex(player), answer);
    }

    public static void remove(UUID uuid) {
        if (results.containsKey(uuid)) {
            results.remove(uuid);
        }
    }

    public static boolean contains(UUID uuid) {
        return results.containsKey(uuid);
    }

    private static int getLastIndex(Player player) {
        Result result = results.get(player.getUniqueId());
        for (int i = 0; i < questions.size(); i++) {
            String answer = result.getResult().get(i);

            if (answer.equalsIgnoreCase("")) {
                return i;
            }
        }

        return -1;
    }

    public static List<Question> getQuestions() {
        return questions;
    }
}
