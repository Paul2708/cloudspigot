package de.paul2708.spigot.listener;

import de.paul2708.spigot.api.PlayerAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 05.06.2017.
 */
public class PlayerChatTabCompleteListener implements Listener {

    // TODO: richtig tabben

    @EventHandler
    public void onChatTabComplete(PlayerChatTabCompleteEvent event) {
        String message = event.getChatMessage().trim();
        List<String> names = new ArrayList<>();

        // Bukkit.broadcastMessage("Nachricht: " + message);

        if (message.equalsIgnoreCase("")) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                String name = PlayerAPI.isNicked(player) ? PlayerAPI.getNick(player) : player.getName();
                names.add(name);
            }
        } else if (event.getChatMessage().contains(" ")) {
            String[] array = event.getChatMessage().split(" ");

            String last = array[array.length - 1];
            if (last.equalsIgnoreCase("")) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    String name = PlayerAPI.isNicked(player) ? PlayerAPI.getNick(player) : player.getName();
                    names.add(name);
                }
            } else {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    String name = PlayerAPI.isNicked(player) ? PlayerAPI.getNick(player) : player.getName();

                    if (startsWith(last, name)) {
                        names.add(name);
                    }
                }
            }
        } else {
            for (Player player : Bukkit.getOnlinePlayers()) {
                String name = PlayerAPI.isNicked(player) ? PlayerAPI.getNick(player) : player.getName();

                if (startsWith(message, name)) {
                    names.add(name);
                }
            }
        }

        event.getTabCompletions().clear();
        event.getTabCompletions().addAll(names);
    }

    private boolean startsWith(String input, String name) {
        if (input.length() > name.length()) {
            return false;
        }

        boolean wrong = false;
        for (int i = 0; i < input.length(); i++) {
            String letter = String.valueOf(input.charAt(i));

            if (!letter.equalsIgnoreCase(String.valueOf(name.charAt(i)))) {
                wrong = true;
            }
        }

        return !wrong;
    }
}
