package de.paul2708.spigot.listener;

import de.paul2708.common.rank.Rank;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.chatlog.ChatlogManager;
import de.paul2708.spigot.image.ImageManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Paul on 11.08.2016.
 */
public class AsyncPlayerChatListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onChat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();

        if (ImageManager.send) {
            e.setCancelled(true);
            return;
        }

        ChatlogManager.log(p.getUniqueId(), e.getMessage(), "CHAT");

        String message = e.getMessage();

        if (count(message, "%") > 5 || count(message, "?") > 10 || count(message, "!") > 10) {
            p.sendMessage(CloudSpigot.TAG + "§cBitte spamme keine Zeichen.");
            e.setCancelled(true);
        }

        message = message.replaceAll("<3", "§c❤§7");
        message = message.replaceAll("❤", "§c❤§7");
        message = message.replaceAll("%", " Prozent");

        if (PlayerAPI.getRank(p) == null) {
            e.setFormat("§f" + p.getName() + " §8» §7" + e.getMessage());
        } else {
            if (PlayerAPI.isNicked(p)) {
                e.setFormat("§a" + "Spieler" + " §7| " + "§a" + PlayerAPI.getNick(p) + " §8» §7" + message);
            } else {
                Rank rank = PlayerAPI.getRank(p);
                e.setFormat(rank.getPrefix() + rank.getTag() + " §7| " + rank.getPrefix() + p.getName() + " §8» §7" + message);
            }
        }
    }

    private int count(String input, String letter) {
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            if (String.valueOf(input.charAt(i)).equalsIgnoreCase(letter)) {
                count++;
            }
        }

        return count;
    }
}
