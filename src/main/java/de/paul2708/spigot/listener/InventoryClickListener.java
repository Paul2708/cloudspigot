package de.paul2708.spigot.listener;

import de.paul2708.common.CloudCommon;
import de.paul2708.common.packet.report.CreateReportPacket;
import de.paul2708.common.packet.report.RemoveReportPacket;
import de.paul2708.common.report.Report;
import de.paul2708.common.report.ReportCause;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.ServerAPI;
import de.paul2708.spigot.report.ReportManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 31.08.2016.
 */
public class InventoryClickListener implements Listener {

    // Create Report
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(InventoryClickEvent e) {
        Player p = ((Player) e.getWhoClicked());

        // Check inventory and items
        Inventory inventory = e.getInventory();
        if (inventory == null || inventory.getName() == null ||
                !ChatColor.stripColor(inventory.getName()).equalsIgnoreCase("Report - Gründe")) {
            return;
        }
        ItemStack item = e.getCurrentItem();
        if (item == null || item.getType() == Material.AIR) {
            return;
        }

        e.setCancelled(true);
        if (item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null) {
            return;
        }

        // Report
        ReportCause cause = ReportCause.getByName(ChatColor.stripColor(item.getItemMeta().getDisplayName()));
        if (cause != null) {
            Object[] data = ReportManager.getData(p);
            if (data == null) {
                p.sendMessage(CloudCommon.TAG + "§cEs ist ein Fehler aufgetreten.");
                e.getView().close();
                return;
            }

            Report report = new Report(p.getName(),
                    (String) data[1],
                    (String) data[2],
                    System.currentTimeMillis(),
                    cause);
            CreateReportPacket packet = new CreateReportPacket(report);
            CloudSpigot.getCloudAPI().getClient().send(packet);

            p.sendMessage(CloudCommon.TAG + "§7Du hast §e" + data[1] + " §7erfolgreich wegen §e" +
                    cause.getName() + " §7reportet.");
            p.sendMessage(CloudCommon.TAG + "§7Falls er gebannt wird, bekommst du eine Benachrichtigung.");

            ReportManager.removeData(p);
            e.getView().close();
        }
    }

    // Report overview
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClickOverview(InventoryClickEvent e) {
        Player p = ((Player) e.getWhoClicked());

        // Check inventory and items
        Inventory inventory = e.getInventory();
        if (inventory == null || inventory.getName() == null ||
                !ChatColor.stripColor(inventory.getName()).equalsIgnoreCase("Report-Verwaltung")) {
            return;
        }
        ItemStack item = e.getCurrentItem();
        if (item == null || item.getType() == Material.AIR) {
            return;
        }

        e.setCancelled(true);
        if (item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null) {
            return;
        }

        // Accept report
        if (e.getAction() == InventoryAction.PICKUP_HALF) {
            Report report = ReportManager.getReport(ChatColor.stripColor(item.getItemMeta().getDisplayName().split(" ")[1]));
            CloudSpigot.getCloudAPI().getClient().send(new RemoveReportPacket(report));
            p.sendMessage(CloudSpigot.TAG + "§7Du hast den Report gelöscht.");
        } else {
            Report report = ReportManager.getReport(ChatColor.stripColor(item.getItemMeta().getDisplayName().split(" ")[1]));
            CloudSpigot.getCloudAPI().getClient().send(new RemoveReportPacket(report));
            ServerAPI.send(p, report.getServerName());
            p.sendMessage(CloudSpigot.TAG + "§7Du übernimmst den Report.");
        }

        e.getView().close();
    }
}
