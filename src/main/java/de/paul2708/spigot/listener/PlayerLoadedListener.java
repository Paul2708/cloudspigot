package de.paul2708.spigot.listener;

import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.event.PlayerLoadedEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Paul on 07.01.2017.
 */
public class PlayerLoadedListener implements Listener {

    @EventHandler
    public void onLoaded(PlayerLoadedEvent e) {
        Player p = e.getPlayer();

        if (PlayerAPI.isNicked(p)) {
            p.sendMessage(CloudSpigot.TAG + "§7Du spielst als §e" + PlayerAPI.getNick(p));
        }
    }
}
