package de.paul2708.spigot.listener;

import de.paul2708.common.packet.server.ServerPlayerPacket;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.chatlog.ChatlogManager;
import de.paul2708.spigot.image.ImageManager;
import de.paul2708.spigot.rank.RankManager;
import de.paul2708.spigot.report.ReportManager;
import de.paul2708.spigot.survey.SurveyManager;
import de.paul2708.spigot.tablist.TablistManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Paul on 31.08.2016.
 */
public class PlayerQuitListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        ChatlogManager.log(p.getUniqueId(), "Server verlassen", "LEAVE");

        RankManager.apply(p, null);
        TablistManager.remove(PlayerAPI.isNicked(p) ? PlayerAPI.getNick(p) : p.getName());

        ReportManager.removeData(p);

        ImageManager.players.remove(p.getUniqueId());

        SurveyManager.remove(p.getUniqueId());

        CloudSpigot.getCloudAPI().getClient().send(
                new ServerPlayerPacket(CloudSpigot.getConfigFile().getName(), "leave", p.getUniqueId()));
    }
}
