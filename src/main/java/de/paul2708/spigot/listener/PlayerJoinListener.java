package de.paul2708.spigot.listener;

import de.paul2708.api.request.Request;
import de.paul2708.common.api.Information;
import de.paul2708.common.nick.Nick;
import de.paul2708.common.packet.server.ServerPlayerPacket;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.chatlog.ChatlogManager;
import de.paul2708.spigot.event.PlayerLoadedEvent;
import de.paul2708.spigot.nick.NickManager;
import de.paul2708.spigot.rank.RankManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Paul on 03.09.2016.
 */
public class PlayerJoinListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1));

        Request.Builder builder = new Request.Builder()
                .identifier("api_login")
                .add(Information.Type.PLAYER_RANK, p.getUniqueId())
                .add(Information.Type.TOKENS, p.getUniqueId());

        if (!CloudSpigot.getConfigFile().isGameServer()) {
            builder.add(Information.Type.PLAY_TIME, p.getUniqueId());
        } else {
            builder.add(Information.Type.NICK, p.getUniqueId());
        }

        builder.handle(response -> {
            if (p.isOnline()) {
                for (PotionEffect effect : p.getActivePotionEffects()) {
                    p.removePotionEffect(effect.getType());
                }

                RankManager.apply(p, PlayerAPI.getRank(p));

                if (PlayerAPI.isNicked(p)) {
                    Nick nick = CloudSpigot.getCloudAPI().getCache().get(Information.Type.NICK, p.getUniqueId());
                    NickManager.apply(p, nick);
                }

                PlayerLoadedEvent event = new PlayerLoadedEvent(p);
                Bukkit.getPluginManager().callEvent(event);
            }
        });

        builder.build().send();

        ChatlogManager.log(p.getUniqueId(), "Server betreten", "JOIN");

        CloudSpigot.getCloudAPI().getClient().send(
                new ServerPlayerPacket(CloudSpigot.getConfigFile().getName(), "join", p.getUniqueId()));
    }
}
