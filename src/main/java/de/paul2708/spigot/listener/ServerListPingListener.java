package de.paul2708.spigot.listener;

import de.paul2708.spigot.util.Settings;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

/**
 * Created by Paul on 08.01.2017.
 */
public class ServerListPingListener implements Listener {

    @EventHandler
    public void onPing(ServerListPingEvent e) {
        String motd = ChatColor.translateAlternateColorCodes('&', Settings.MOTD);
        e.setMotd(motd);
    }
}
