package de.paul2708.spigot.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 25.08.2016.
 */
public class ConfigFile extends BasicFile {

    public ConfigFile() {
        super("plugins/CloudSpigot/config.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("default cloud spigot config");

        set("server_name", "TestServer");
        set("ip", "localhost");
        set("port", 30001);
        set("listed_server", false);
        set("game_server", true);
        set("motd", "defaul cloud spigot motd");
        set("max_players", 10);
        set("packet_ip", "localhost");
        set("packet_port", 2000);

        save();
    }

    public boolean isListed() {
        return get("listed_server");
    }

    public boolean isGameServer() {
        return get("game_server");
    }

    public String getName() {
        return get("server_name");
    }

    public String getMotd() {
        return get("motd");
    }

    public int getMaxPlayers() {
        return get("max_players");
    }

    public String getIp() {
        return get("ip");
    }

    public int getPort() {
        return get("port");
    }

    public String getPacketIp() {
        return get("packet_ip");
    }

    public int getPacketPort() {
        return get("packet_port");
    }
}
