package de.paul2708.spigot;

import de.paul2708.api.CloudAPI;
import de.paul2708.common.game.GameState;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.client.ClientLoginPacket;
import de.paul2708.common.packet.server.ServerRegistrationPacket;
import de.paul2708.spigot.command.*;
import de.paul2708.spigot.file.ConfigFile;
import de.paul2708.spigot.listener.*;
import de.paul2708.spigot.network.CommandPacketHandler;
import de.paul2708.spigot.network.PlayerPacketHandler;
import de.paul2708.spigot.network.RankPacketHandler;
import de.paul2708.spigot.network.ReportPacketHandler;
import de.paul2708.spigot.util.ItemManager;
import de.paul2708.spigot.util.Settings;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Paul on 11.08.2016.
 */
public class CloudSpigot extends JavaPlugin {

    // TODO: add basic scoreboard
    // TODO: use reflections

    public static final String TAG = "§cSystem§8│ §r";

    @Getter
    private static CloudSpigot instance;

    @Getter
    private static ConfigFile configFile;

    @Getter
    private static CloudAPI cloudAPI;

    @Override
    public void onLoad() {

    }

    @Override
    public void onEnable() {
        CloudSpigot.instance = this;

        // File
        CloudSpigot.configFile = new ConfigFile();

        // Commands
        new ClearChatCommand();
        new GameModeCommand();
        new JoinProperServerCommand();
        new SpeedCommand();
        new TeleportCommand();
        new TestCommand();
        new TakeReportCommand();
        new OpenBookCommand();

        // Listener
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new AsyncPlayerChatListener(), this);
        pluginManager.registerEvents(new InventoryClickListener(), this);
        pluginManager.registerEvents(new PlayerJoinListener(), this);
        pluginManager.registerEvents(new PlayerQuitListener(), this);
        pluginManager.registerEvents(new PlayerLoadedListener(), this);
        pluginManager.registerEvents(new ServerListPingListener(), this);
        // pluginManager.registerEvents(new PlayerChatTabCompleteListener(), this);

        // Inventories
        ItemManager.initializeReasonInventory();
        ItemManager.initializeReportInventory();

        // Settings
        Settings.MOTD = CloudSpigot.getConfigFile().getMotd();
        if (Settings.GAME_STATE == null) {
            Settings.GAME_STATE = GameState.NONE;
        }
        Settings.CAN_JOIN = true;

        // Client
        CloudSpigot.cloudAPI = new CloudAPI("server_" + configFile.getName());
        CloudSpigot.cloudAPI.connect(configFile.getPacketIp(), configFile.getPacketPort(),
                new RankPacketHandler(), new ReportPacketHandler(), new CommandPacketHandler(), new PlayerPacketHandler());

        CloudSpigot.cloudAPI.getClient().send(new ClientLoginPacket(ClientType.SPIGOT.getId(), configFile.getName()));
        CloudSpigot.cloudAPI.getClient().send(new ServerRegistrationPacket(
                configFile.getName(),
                configFile.getIp(),
                configFile.getPort(),
                configFile.isListed(),
                configFile.isGameServer(),
                configFile.getMotd(),
                configFile.getMaxPlayers()
        ));
    }

    @Override
    public void onDisable() {

    }

}
