package de.paul2708.spigot.image;

import de.paul2708.common.rank.Rank;
import de.paul2708.spigot.util.imagemessage.ImageChar;
import de.paul2708.spigot.util.imagemessage.ImageMessage;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 08.09.2016.
 */
public class ImageManager {

    public static Map<UUID, String> players = new ConcurrentHashMap<>();

    public static boolean send = false;

    public static void sendInfo(String uuid, String target, Rank rank, String server) {
        try {
            BufferedImage image = ImageIO.read(new URL("https://crafatar.com/avatars/" + uuid + ".png"));
            ImageMessage message = new ImageMessage(image, 8, ImageChar.BLOCK.getChar());
            message.appendText("", "", "", rank.getPrefix() + target + " §7spielt auf §e" + server, "", "", "", "");

            send = true;
            int i = 0;
            for (String line : message.getLines()) {
                if (i == 4) {
                    TextComponent text = new TextComponent( line + "§7Klicke hier um zu joinen!");
                    text.setHoverEvent(new HoverEvent( HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Spiele mit: " + server).create() ) );
                    text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/joinproperserver"));

                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.spigot().sendMessage(text);
                    }
                } else {
                    Bukkit.broadcastMessage(line);
                }

                i++;
            }

            for (Player player : Bukkit.getOnlinePlayers()) {
                players.put(player.getUniqueId(), server);
            }

            send = false;
        } catch (IOException e) {
            e.printStackTrace();
            send = false;
        }

    }
}
