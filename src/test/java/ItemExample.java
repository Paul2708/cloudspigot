import de.paul2708.spigot.api.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 01.02.2017.
 */
public class ItemExample {

    public static void main(String[] args) {
        ItemStack item = new ItemBuilder()
                .type(Material.DIAMOND_SWORD)
                .amount(1)
                .enchant(Enchantment.DAMAGE_ALL, 1)
                .name("§bDia-Schwert")
                .description("§7Das Item ist krass.")
                .description("§7Zweite Reihe :3")
                .build();
    }
}
