import de.paul2708.spigot.api.BasicCountdown;

/**
 * Created by Paul on 01.02.2017.
 */
public class CountdownExample extends BasicCountdown {

    public CountdownExample(int time) {
        super(time);
    }

    @Override
    public void handle(int time) {
        if (time % 10 == 0) {
            System.out.println("Countdown-Stopp in " + time + " Sekunden.");
        }
        if (time == 0) {
            System.out.println("Der Countdown wurde gestoppt.");
        }
    }

    @Override
    public void cancel() {
        super.cancel();

        System.out.println("Der Countdown wurde gestoppt oder abgebrochen.");
    }

    public static void main(String[] args) {
        CountdownExample example = new CountdownExample(60);

        if (!example.isRunning()) {
            example.start();
        }
    }
}
