import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.BasicCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Paul on 01.02.2017.
 */
public class CommandExample extends BasicCommand {

    public CommandExample(JavaPlugin plugin, String command, int permissionLevel, List<String> aliases) {
        super(plugin, command, permissionLevel, aliases);
    }

    @Override
    public void run(Player p, String[] args) {
        if (args.length == 0) {
            p.sendMessage("Du hast den Command ohne Argumente ausgeführt.");
        } else {
            String message = "";
            for (int i = 0; i < args.length; i++) {
                message += args[i] + " ";
            }

            p.sendMessage("Du hast folgende Argumente angegeben: ");
            p.sendMessage(message);
        }
    }

    public static void main(String[] args) {
        // Register command
        CommandExample example = new CommandExample(CloudSpigot.getInstance(),
                "test", 0, Arrays.asList("argstest"));
    }
}
